
ifdef CONFIG_CONTRIB_TESTING_PYTHON_BINDINGS_TESTAPP

# Targets provided by this project
.PHONY: rtlib_pytestapp clean_rtlib_pytestapp

# Add this to the "contrib_testing" target
testing: rtlib_pytestapp
clean_testing: clean_rtlib_pytestapp

MODULE_CONTRIB_TESTING_PYTHON_BINDINGS_TESTAPP=contrib/testing/rtlib-pytestapp

rtlib_pytestapp: external
	@echo
	@echo "==== Building RTLib Python Bindings Test Application ===="
	@echo " Sysroot      : $(PLATFORM_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@[ -d $(MODULE_CONTRIB_TESTING_PYTHON_BINDINGS_TESTAPP)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_CONTRIB_TESTING_PYTHON_BINDINGS_TESTAPP)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(MODULE_CONTRIB_TESTING_PYTHON_BINDINGS_TESTAPP)/build/$(BUILD_TYPE) && \
		cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_CONTRIB_TESTING_PYTHON_BINDINGS_TESTAPP)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_rtlib_pytestapp:
	@echo
	@echo "==== Clean-up RTLib Python Bindings Test Application ===="
	@[ ! -f $(BUILD_DIR)/usr/bin/bbque-pytestapp ] || \
		rm -f $(BUILD_DIR)/etc/bbque/recipes/BbqRTLibPythonBindingsTestApp*; \
		rm -f $(BUILD_DIR)/usr/bin/bbque-pytestapp*
	@rm -rf $(MODULE_CONTRIB_TESTING_PYTHON_BINDINGS_TESTAPP)/build
	@echo

else # CONFIG_CONTRIB_TESTING_PYTHON_BINDINGS_TESTAPP

rtlib_pytestapp:
	$(warning rtlib_pytestapp module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_CONTRIB_TESTING_PYTHON_BINDINGS_TESTAPP
